var express = require('express');
var app = express();

//Aca es donde vamos a tener los metodos que usaremos
const conexionesRoutes = require('./api/routes/conexiones');

app.use('/conexion', conexionesRoutes.router);

module.exports = app;