const express = require('express');
const router = express.Router();
var request = require('request');
var env = require('../../config.js');

var app = express();

router.get('/up', (req, res, next) => {
    res.status(200).json({
        message: 'Servernode UP!'
    });
});

router.post('/', (req, res, next) => {
    if (req.body.query) {
        //Hacemos algo
    }
});

router.put('/', (req, res, next) => {
    //Hacemos algo
});

router.delete('/', (req, res, next) => {
    //Hacemos algo
});

function llamada(error, response, body) {
    if (response) {
        if (response.statusCode == 200) {
            //esta IF ocurre cuando se conecto con la base de datos y retorno una respuesta OK
            console.log(response.statusCode + " La instruccion se proceso exitosamente");
            this.respuestadata = body.data;
            return (this.respuestadata);
        } else if (response.statusCode == 400) {
            console.log("Status: " + response.statusCode);
            console.log("ERROR 400: Existen problemas en la estructura del header o la estructura de la query");
            console.log("Error en la llamada a DB");
            console.log(error);

            return 400;
        } else if (response.statusCode == 401) {
            console.log("Status: " + response.statusCode);
            console.log("ERROR 401: Error de autorización, user o pass de la DB incorrectos");
            console.log("Error en la llamada a DB");
            console.log(error);

            return 401;
        } else if (response.statusCode == 404) {
            console.log("Status: " + response.statusCode);
            console.log("ERROR 404: El req.post no apunta a un endpoint encontrable");
            console.log("Error en la llamada a DB");
            console.log(error);

            return 404;
        } else {
            console.log("Status: " + response.statusCode);
            console.log("ERROR: Hay un error pero no sé que pudo pasar :)");
            console.log("Error en la llamada a DB");
            console.log(error);
            return 500;
        }
    }
}

module.exports = { router, llamada };