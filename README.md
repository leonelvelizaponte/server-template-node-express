# server-template-node-express

Este es el patron inicial para comenzar a programar un servidor con Nodejs + express

Para ejecutar el server, se tiene que considerar:

- Instalar nodemon via npm es una forma simple de tener levantado el server actualizado con cada cambio
- Tener instalado nodejs y npm

#Instrucciones:

1. Bajar el proyecto
2. Ejecutar npm install
3. Cambiar el numero de puerto si fuera necesario en el archivo /config.js
4. Ejecutar "nodemon server.js"